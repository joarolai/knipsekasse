

module peg(th = 2, r=2, co=false)
{
    gt = 1.95;

    difference(){
        cylinder(r=r, h=th);
        if( co == false ) {
            translate([0,0,1]) cylinder(r=gt/2, h=th);
        }
    }
}

module ahole(th = 2, r= 2, l=10)
{
    translate([-r,0,0]) cube([2*r,l-2*r,th]);
    cylinder(r=r, h=th);
    translate([0,l-2*r,0]) cylinder(r=r, h=th);
}


module slot(th=2, r=2, wt=2, l=12, co=false)
{
    difference(){
        translate([0,0,0]) ahole(th=th, r=r+wt, l=l+2*wt);
        if ( co == false) {
            translate([0,0,-0.5]) ahole(th=th+1, r=r, l=l);
        }
    }
}


module fixtures(gd=2, d=2, th=2, wt=2, l=2, co=false)
{
    hd = 4.8;
    hr = hd/2;

    translate([0,0,0]) peg(th=th, r=d/2, co=co);
    translate([gd,0,0]) peg(th=th, r=d/2, co=co);
    translate([gd/2,-d/2+hr,0]) slot(th = th, r= hr, wt=wt, l=l ,co=co);
}

module sidecutouts(d=2, th=2, m=0)
{
    translate([0,d/2,-0.5]) mirror([m,0,0]) rotate([0,0,30.1]) cube([40,10,th+1]);
    translate([0,-d/2,th+0.5]) mirror([m,0,0]) rotate([180,0,-8]) cube([40,10,th+1]);
}

module gate()
{
    d = 6.85;

    hh = 0.2;
    th = 6.2;
    th2 = th-hh;

    gd = 25.72 + 1.95;

    wt = 2;
    l = 12.9;


    w = gd;
    h = l + 2*wt;

    difference(){
        color("pink") translate([0,-d/2 - wt,0]) cube([w,h,th2]);
        translate([0,0,-hh]) fixtures(gd=gd, d=d, th=th+hh, wt=wt, l=l, co=true);

        sidecutouts(d=d,th=th2);

        translate([gd,0,0]) {
            sidecutouts(d=d,th=th2, m=1);
        }
    }


    fixtures(gd=gd, d=d, th=th, wt=wt, l=l);
}

// Test
$fn = 64;
scale(0.84,0.84,0.84) gate();

