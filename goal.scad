

module body_cut(x = 29.3, y=18.6)
{
    z = 100;    // Infinite
    color("orange") cube([x,y,z]);
}

module cut_edge(x=2.5, y=3, th=2.25)
{
    a = atan2(x,y);
    translate([2*x,-y,-0.5]) color("red") rotate([0,0,a]) cube([x,2*y,th+1]);
    translate([0,-y,-0.5]) color("blue") mirror([1,0,0]) rotate([0,0,a]) cube([x,2*y,th+1]);
}

module edge(x = 29.3)
{
    h = 2;
    r = x/2;
    
    dd = 3.35;
    dd2 = 6.5;
    dd3 = 8.5;
    lr = 9.86;
    dds = 1.55;
     
    difference(){
        cylinder(r=r, h=h);
        translate([-r,0,-0.5]) cube([x,r,h+1]);
        translate([-r,-x-lr,-0.5]) cube([x,x,h+1]);
        translate([0,-dd/2 - dds,-0.5]) cylinder(r=dd/2,h=h+1);
        translate([-dd/2,-100 - dds-dd/2,-0.5]) cube([dd,100, h+1]);
        translate([0,-dds-dd/2,dd2/2]) sphere(r=dd2/2);
        translate([0,-dds-dd/2,dd2/2]) rotate([90,0,0]) cylinder(r=dd2/2, h=100);
    }
}

module face(xbc = 26, f=1, ff=1)
{
    px = 5 * f;
    py = 3 * f;

    y = 35 * ff;
    x = xbc * f + 2 * px;
    th = 2.25;

    // Sticker cavity
    xs = 33*f;
    ys = 6*ff;
    ds = 1;

    y0 = 11.8 * ff;
    x0 = px;
    
    x1 = x0/2;
    y1 = py;

    x2 = xs;
    y2 = ys;
    d2 = ds;
    ox2 = (x-x2)/2;
    oy2 = 1.2;

    xx = x - 2*x0;
    yy = y - y0;

    translate([-px,-oy2,0]){
        difference() {
            cube([x,y,th]);
            translate([x0,y0,-0.5]) color("green") cube([xx, yy+0.5, th+1]);
            
            translate([0,y,0]) cut_edge(x1, y1, th);
            translate([x-x0,y,0]) cut_edge(x1, y1, th);
            
            translate([ox2,oy2,th-d2]) color("red") cube([x2,y2, d2+0.5]);
        }    
    }
}

module cut_body(x=29.3, y=76, z = 26.7, f=1, tweek=0)
{  
    _x = x*f;
    y1 = 18.6+6;
    z1 = 9.45;
    a1 = 61.1;

    r = 84 - tweek;
   
    translate([0,-0.5,-2]){
        
        difference(){
            cube([_x,y,z+6]);
            translate([-0.5,0, r + z1 - 2.5]) rotate([90,0,90]) cylinder(r=r, h=_x+1);
        }

        difference(){
            cube([_x,y,z+6]);
            translate([-0.5, -0.5, z1-4.4]) body_cut(_x+1, y1+0.5);
            translate([-0.5, y1, z1]) mirror([0,1,0]) rotate([a1,0,0]) body_cut(_x+1, y1+0.5);
        }
        
        ll = 16.68+0.5;
        
        zz = z1 + 2 - 2;
        
        translate([0,0,0])   cube([3.2, ll,zz]);
        translate([_x-3.2,0,0]) cube([3.2, ll,zz]);
        
        translate([6.2,0,0]) cube([2.7, ll,zz]);
        translate([_x-6.2-2.7,0,0]) cube([2.7, ll,zz]);
    }
}


module slit_cut()
{
    sx = 1; 
    sy = 50;
    sz = 0.8;

    cube([sx,sy,sz]);
    mirror([0,0,1]) rotate([0,40,0]) cube([sx,sy,sz]);
    
    translate([0,40,-3]) rotate([40,0,0]) cube([1, 1, 4]);
    translate([0,41,-3]) rotate([40,0,0]) cube([1, 1, 4]);
    translate([0,42,-3]) rotate([40,0,0]) cube([1, 1, 4]);
    translate([0,43,-3]) rotate([40,0,0]) cube([1, 1, 4]);
    translate([0,44,-3]) rotate([40,0,0]) cube([1, 1, 4]);
}

module body(x=29.3, y=76, y2=70 , z=26.7, f=1, ff=1, tweek=0)
{  
    y1 = 18.6;
    z1 = 9.45;
    a1 = 64.8;
    a2 = 60 - tweek;

    ybb = 38.4;

    sth = 0.9+1;    // Add one here to compensate for thicker wall
    stp = 2.6-0.8;
    
    yy = (y - y2)*ff;

    difference(){
        union(){
            cube([x,y2,z]);
            translate([0,y2,0]) cube([x,yy,z]);
        }
        translate([-0.5, -0.5, z1]) body_cut(x+1, y1+0.5);
        translate([-0.5, y1, z1]) mirror([0,1,0]) rotate([a1,0,0]) body_cut(x+1, y1+0.5);
        translate([-0.5, ybb, 0]) rotate([-a2,0,0]) body_cut(x+1, y1+10);
        
        translate([sth, -0.5, stp]) slit_cut();
        translate([x-sth, -0.5, stp]) mirror([1,0,0]) slit_cut();
    }

    yl = 30.44*f; // This must align with face height

    xl = x;
    zl = 2.3;
    
    dyl = y2;
    dzl = z - zl;
    
    translate([0, dyl, dzl]) cube([xl, yl, zl]);
}

module goal_assembly(f=1, ff=1, tweek1=0, tweek2=0)
{
    xbc = 26.25;           // Width of channel
    tb = 3 * f;          // Thicknes of body wall

    yb = 76;               // Length of body
    yb2 = 76 - 30.44;      // Length of unscalable body

    zbh = 26.7 - 2.25;     // Height of body (highest point)
    zbl = 9.45;            // Hight of body (lowest point)

    xb = xbc*f + 2 *tb;    // Width of body (must be calculated to guarantee alignment)

    dxf = tb;               // X offset of face plate relative to body
    dxc = tb;

    dyf = yb2;
    dzf = zbh;
    
    difference(){
        union(){
            body(xb, yb, yb2, zbh, f=f, ff=ff, tweek=tweek2);
            translate([dxf, dyf, dzf]) face(xbc=xbc, f=f, ff=ff);
        }
        translate([dxc,0,0]) color("orange") cut_body(xbc,yb+10,zbh, f=f,ff=ff, tweek=tweek1);
    }
    
    translate([xb/2,0,zbl]) rotate([0, 180, 0]) edge(x=xb);
    
    //translate([xb + 10,0,0]) cut_body(xbc,yb+10,zbh);
}

module goal84x()
{
    f = 0.84;
    ff = 1;

    goal_assembly(0.84,1);
}

module goal84xy()
{
    f = 0.84;
    ff = f;     // Set to f to scale y axis, set to 
    tweek1 = 4; // Tweek channel lip thicknes: ff = 1 -> tweek1 = 0 , ff=0.84 -> tweek1 = 4
    tweek2 = 4; // Tweek backside angle:  ff = 1 -> tweek1 = 0 , ff=0.84 -> tweek1 = 4

    goal_assembly(f,ff,tweek1, tweek2);
}

module goal()
{
    goal_assembly();
}

module goals(){
    translate([0*40,0,0]) goal84xy();
    //translate([1*40,0,0])  goal();
    //translate([2*40,0,0]) goal84x();
}

module goali()
{
    difference(){
        goal();
        color("purple") translate([20,-1,-1]) cube([10,100,100]);
    }
}

// Test
$fn = 164;
goals();
