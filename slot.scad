

module co_slant(a, lx, ly, lz, rotation = 0)
{
    color("green")  translate([lx,0,0]) rotate([0, -a, rotation]) translate([0,-0.5,0]) cube([lx,ly+1,lz]); 
}
module co_plunger(lx,ly,lz)
{
    d = 2;

    difference() {
        translate([-0.5,-0.5,0]) cube([lx+0.5,ly+1,lz+0.5]);
        translate([lx-d,-1,-0.5]) cube([d+0.5,ly+2,d+0.5]);
    }

    // Small curve on inside
    translate([lx-d,-0.5,d]) rotate([-90,0,0]) cylinder(r=d, h = ly+1);
}

module plunger(co=0, sco = 0)
{
    // Slot
    sy = 11 + 2*sco;
    sx = 2.6 + sco;

    lx = 28.5;
    ly = 24;
    lzz = 20;
    lz = lzz+co;
    
    // Slant
    x1 = -20;
    y1 = 0;
    z1 = 0;
    lx1 = -x1;
    ly1 = ly;
    lz1 = lz*2; // Multiply by to to be sure it is higher than lz
    a1  = 10;

    // Invers curve at the top of plunger
    x2 = lz / tan(90-a1);
    y2 = y1;
    z2 = lz;

    r2 = 3;

    difference(){
        union(){
            difference(){    
                union(){
                    cube([lx - sx+sco,ly,lz]);
                    translate([lx-sx+sco,(ly-sy)/2-1,0]) cube([sx,sy,lz]);
                }

                if ( co == 0 && sco == 0) {
                    translate([x1,y1+ly1,z1]) co_slant(a1, lx1,ly1,lz1, 180);
                }

                translate([x2-100,y2-0.5,z2-r2]) cube([r2+100, ly+1, r2+0.5]);
            }

            dr = 0.58;

            translate([x2+r2,y2,z2-r2-dr]) rotate([-90,0,0]) cylinder(r=r2+dr, h=ly);
        }

        // Cutout for tiny edge
        if ( co == 0 && sco == 0) {
            translate([-0.1,-0.1,0.5]) cube([lx+0.2,0.6,lz]);
            translate([-0.1,ly-0.5,0.5]) cube([lx+0.2,0.6,lz]);
        }

        if ( co == 0){
            translate([lx/2,ly/2,-1]) cylinder(r=6+3,h=lzz-2);
        } else {
            translate([lx/2,ly/2,-1]) cylinder(r=6,h=lzz-4);
        }
    }

    ll = 20;

    if (co ) {
        translate([lx/2,ly/2,-ll]) cylinder(r=4+0.2,h=ll+lzz);
    } else {
        translate([lx/2,ly/2,-ll]){
            difference() {
                cylinder(r=4-0.2,h=ll+lzz);
                translate([0,0,-5]) cylinder(r=2.5,h=ll+lzz);
            }
        }
    }
}

module frame()
{
    lx = 72;
    ly = 30;
    lz = 37;

    a1  = 10;

    // Cutout for plunger
    x0  = 0;
    y0  = 0;
    z0  = 19;
    lx0 = 19 / tan(90-a1) + 28.5 + 1.5 - 2.6;

    ly0 = ly;
    lz0 = lz - z0;

    // Cutout for slanted sides
    x1 = -20;
    y1 = 0;
    z1 = 0;
    lx1 = -x1;
    ly1 = ly;
    lz1 = lz*2; // Multiply by to to be sure it is higher than lz

    x2 = x1;
    y2 = y1;
    z2 = z1;
    a2 = 4;
    lx2 = lx1;
    ly2 = lx;
    lz2 = lz1;

    difference(){
        cube([lx,ly,lz]);

        // Cutout for plunger
        color("pink") translate([x0,y0,z0]) co_plunger(lx0, ly0, lz0);

        // Cutout on sides
        translate([x1,y1+ly1,z1]) co_slant(a1, lx1,ly1,lz1, 180);
        translate([x1+lx,y1,z1])  co_slant(a1, lx1,ly1,lz1, 0);
        rotate([0,0,90]) translate([x1,y2,z2]) co_slant(a2, lx2,ly2,lz2, 180);
        rotate([0,0,90]) translate([x1+ly,y2-lx,z2]) co_slant(a2, lx2,ly2,lz2, 0);

        // Cutout for corner
        r3 = 5;
        x3 = lx - lz / tan(90-a1);
        d3 = 0.8;

        difference(){
            translate([x3-r3+d3,-0.5, lz-r3+1]) cube([50,ly+1,50]);
            translate([x3-r3+d3,ly+0.5,lz-r3]) rotate([90,0,0]) cylinder(r=r3, h=ly+1);
        }
    }
}

module screewhole(){
    sh = 5.5;
    d = 7.8;
    di= 4;

    translate([0,0,-sh]) {
        difference(){
            cylinder(r=d/2,h=sh);
            translate([0,0,-0.5]) cylinder(r=di/2,h=sh+1);
        }
    }
}

module bottom() {
    rh = 9.6;
    rr = 1.5;

    cr = (25.3/2)+rr/2 ;
    ct = 4;

    lby = 30;
    lbx = 72;

    lcy = 29 - 1.5;

    x = 1;

    dd = 2; // Wall thickness

    bl = 2*cr+2*x + 2*dd;
    bw = ct + 2*dd;
    bh = 6.5;

    ly = 20;
    so = 7;

    translate([50.5-cr-x-dd,ly+3-dd,-bh]) cube([bl,bw,bh]);

    translate([lbx-so,lby/2,0]) screewhole();
    translate([so,lby*3/4,0]) screewhole();

}


module inserts() {
    rh = 9.6;
    rr = 1.5;

    cr = (25.3/2)+rr/2 ;
    ct = 4;

    lcy = 29 - 1.5;

    x = 1;

    dd = 2; // Wall thickness

    bl = 2*cr+2*x + 2*dd;
    bw = ct + 2*dd;
    bh = 6.5;

    ly = 20;

    px = 50.5-cr+4;
    py = ly+3-2*dd+bw-1;
    pz = -bh;

    translate([px,py,pz]) cube([4,1,42]);
    translate([px+cr+1.5,py,pz]) cube([4,1,42]);
}

module co_coin()
{
    lz = 37;
    ly = 20;

    r = 13;

    difference() {
        cube([50,ly,lz]);
        translate([-0.5,-0.5,-0.5]) cube([r+0.5,ly+1, r+0.5]);
    }

    translate([r,ly,r]) rotate([90,0,0]) cylinder(r=r, h=20);

    rh = 9.6;
    rr = 1.5;

    cr = (25.3/2)+rr/2 ;
    ct = 4;

    lcy = 29 - 1.5;

    x = 1;

    translate([15-cr-x,ly+3,-20]) cube([2*cr+2*x,ct, 20+15]);
    translate([15,ly+3+ct,15+1]) rotate([90,0,0]) cylinder(r=cr+x,h=ct);

    difference(){
        union(){
            translate([15,lcy-ct,15]) rotate([90,0,0]){
                cylinder(r=cr+1.5, h=lcy-ct);
                translate([0,0,-9.5]) cylinder(r=rh/2, h=10);
            }
        }

        translate([15,ly+rr,15]) {
            rotate([90,0,0]) {
                rotate_extrude(convexity = 10, $fn = 100)
                translate([cr+1.5, 0, 0])
                circle(r = rr, $fn = 100);
            }
        }
    }
}

module body()
{
    lxf = 72;
    lyf = 30;
    lzf = 37;

    lxp = 28;
    lyp = 24;
    lzp = 20;

    xp = 6;
    yp = (lyf - lyp)/ 2;
    zp = lzf-lzp;

    a = 10;
    zc = 19;
    xc = 1.5 + zc / tan(90-a);
    yc = 2;

    difference() {
        union(){
            frame();
            bottom();
        }
        
        color("pink") {
            translate([xc,yp,yc])plunger(co=100, sco = 0.5);
            translate([35.5,0,5]) co_coin();
        }
    }

    color("red") inserts();
}

module slot( print=false, body=true, plunger=true)
{
    lxf = 72;
    lyf = 30;
    lzf = 37;

    lxp = 28;
    lyp = 24;
    lzp = 20;

    xp = 6;
    yp = (lyf - lyp)/ 2;
    zp = lzf-lzp;

    a = 10;
    zc = 19;
    xc = 1.5 + zc / tan(90-a);
    yc = 2;

    if ( body ) {
        body();
    }

    if ( plunger ) {
        if ( print ) {
            translate([-2,0,20]) rotate([0,180,0]) plunger();
        } else {
            color("red") translate([xc,yc, zc - 2]) plunger();
        }
    }
}

// Test
$fn = 64;
scale(0.84,0.84,0.84) slot( print = true, body=false, plunger=true);
